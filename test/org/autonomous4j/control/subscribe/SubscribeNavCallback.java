package org.autonomous4j.control.subscribe;

import org.eclipse.paho.client.mqttv3.*;

import utils.DataWriteFile;
import utils.Imagen;
import utils.PanelImagen;
import utils.Serializar;

import java.awt.image.BufferedImage;
import java.awt.image.DataBuffer;
import java.awt.image.DataBufferByte;
import java.io.File;
import java.io.IOException;
import java.lang.Throwable;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Objects;

import javax.swing.JLabel;

public class SubscribeNavCallback implements MqttCallback {
	
	private JLabel labelNav;
	private int i =0;
	DataWriteFile dw = new DataWriteFile();
	private String[] dataRoll;
	private String[] dataYaw;
	private String[] dataPitch;
	private String[] dataSpeedx;
	private String[] dataSpeedy;
	private String[] dataSpeedz;
	private ArrayList axisnav;
	private static boolean flag = true;
	
	public SubscribeNavCallback(JLabel labelNav,ArrayList axisnav) {	
		this.labelNav = labelNav;
		this.axisnav=axisnav;
	}
	

     @Override
     public void connectionLost(Throwable cause) {}

     @Override
     public void messageArrived(String topic, MqttMessage message) {
    	 axisnav.add(message.toString());
    	 System.out.println(axisnav.size());
    	 if(axisnav.size()==100) {
    		 System.out.println("imprimiendo...");
    		 dw.writeData(axisnav,topic);
    	 }
    	 
    	 
    	 
    	/* if(i!=10000 && flag) {
    		 System.out.println("muestreo: "+i);
        	 if(topic.equals("a4jnavdata/roll")){
        		 dataRoll[i]=message.toString();
        		 i++;
        	 }if(topic.equals("a4jnavdata/yaw")){
        		 dataYaw[i]=message.toString();
        		 i++;
        	 }if(topic.equals("a4jnavdata/pitch")){
        		 dataPitch[i]=message.toString();
        		 i++;
        	 }if(topic.equals("a4jnavdata/speedx")){
        		 dataSpeedx[i]=message.toString();
        		 i++;
        	 }if(topic.equals("a4jnavdata/speedy")){
        		 dataSpeedy[i]=message.toString();
        		 i++;
        	 }if(topic.equals("a4jnavdata/speedz")){
        		 dataSpeedz[i]=message.toString();
        		 i++;
        	 }
    	 }
         if(i==10000 && flag){
        	 dw.writeData(dataRoll,"roll");
        	 dw.writeData(dataPitch,"pitch");
        	 dw.writeData(dataYaw,"yaw");
        	 dw.writeData(dataSpeedx,"speedx");
        	 dw.writeData(dataSpeedy,"speedy");
        	 dw.writeData(dataSpeedz,"speedz");
        	 flag=false;
         }*/
    	 labelNav.setText(message.toString());
    	 }

     @Override
     public void deliveryComplete(IMqttDeliveryToken token) {}

     
     
     
}
