package org.autonomous4j.control.subscribe;

import javax.swing.JLabel;

import org.eclipse.paho.client.mqttv3.*;

import utils.PanelImagen;

public class SubscriberVideo {

     public static final String BROKER_URL = "tcp://localhost:1883";
     private MqttClient client;
     private PanelImagen panelImagen;
 	 private String propiedad;
 	
     public SubscriberVideo(PanelImagen panelImagen, String propiedad) {
    	  this.propiedad=propiedad;
          this.panelImagen = panelImagen;
          String clientId = "demonio"+propiedad;
          try {
              client = new MqttClient(BROKER_URL, clientId);
          }
          catch (MqttException e) {
              e.printStackTrace();
              System.exit(1);
          }
     }

     public void start() {
          try {        	  //"a4jnavdata/roll"};
              client.setCallback(new SubscribeVideoCallback(panelImagen));              
              client.connect();
              client.subscribe("a4jvideodata/"+propiedad);              
          }
          catch (MqttException e) {
              e.printStackTrace();
              System.exit(1);
          }
     }
}
