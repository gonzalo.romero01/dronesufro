package org.autonomous4j.control;

import org.lwjgl.LWJGLException;
import org.lwjgl.input.Controller;
import org.lwjgl.input.Controllers;


public class JoysticControl {
	private static Controller joystickController;
	
	public void activaJoystick() {
		try {
			Controllers.create();
		} catch (LWJGLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Controllers.poll();
		configuraJoystick();		
	}
	
	public void configuraJoystick() {
		for (int i = 0; i < Controllers.getControllerCount(); i++) {
			joystickController = Controllers.getController(i);
			System.out.println(joystickController.getName());
			joystickController = Controllers.getController(i);	
		}
	}
	
	public void takeOffJoystick(Brain br){
		joystickController.poll();
		boolean isButtonTri = joystickController.isButtonPressed(0);
		while(isButtonTri) {
			br.takeoff();
			br.stay();			
			break;
		}
	}
	
	public void landJoystick(Brain br) {
		joystickController.poll();
		boolean isButtonX = joystickController.isButtonPressed(2);
		while(isButtonX) {
			br.land();			
			break;
		}
	}
	
	public void camFrontalHD(Brain br) {
	boolean isButtonCir=joystickController.isButtonPressed(1);
	while(isButtonCir) {
		//br.viewFrontalCamera();
		break;
	}
	}
	public void camBot480p(Brain br) {
	boolean isButtonCua=joystickController.isButtonPressed(3);
	while(isButtonCua) {
		br.viewBackCamera();
		break;
	}
	}
	public void foward(Brain br) {
	while(joystickController.getAxisValue(2)>0.5) {
		br.forward(500);
		br.stay();
		break;
	}
	}
	public void backward(Brain br) {
	while(joystickController.getAxisValue(2)<-0.5) {
		br.backward(500);
		br.stay();
		break;
	}
	}
	public void goRigth(Brain br) {
	while(joystickController.getAxisValue(3)>0.5) {
		br.goRight(300);
		br.stay();
		break;
	}
	}
	public void goLefth(Brain br) {
	while(joystickController.getAxisValue(3)<-0.5) {
		br.goLeft(400);
		br.stay();
		break;
	}
	}
	public void up(Brain br) {
	while(joystickController.getAxisValue(0)>0.5) {
		br.up(600);
		br.stay();
		break;
	}
	}
	public void down(Brain br) {
	while(joystickController.getAxisValue(0)<-0.5) {
		br.down(600);
		br.stay();
		break;
	
	}	}
	public void turnRight(Brain br) {
	while(joystickController.getAxisValue(1)>0.5) {
		br.move(0, 0, 0, 0.1f);
		br.stay();
		break;
	}
	}
	public void turnLeft(Brain br) {
	while(joystickController.getAxisValue(1)<-0.5) {
		br.move(0, 0, 0, 0.1f);
		br.stay();
		break;
	}
	}
	
	public void destruyeJoystick() {
		
			Controllers.destroy();
		
	}
	

}
