package gui;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JPanel;

import org.autonomous4j.control.Brain;

import utils.KeyCommand;

public class KeyPanel extends JPanel{
	/**
	 * 
	 */
	private static final long serialVersionUID = 5725876298826044725L;
	
	private Brain b;
	private KeyCommand kc;
	    
    public KeyPanel(Brain b) {
		this.b=b;
		kc = new KeyCommand(b);
		addKeyListener(kc);
	}

}
