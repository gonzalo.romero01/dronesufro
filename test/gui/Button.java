/* Esta clase es heredero de la clase button de javax.swing solo que aqui agrego
 * la facultad del key listener.
 */

package gui;
 
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JButton;

import org.autonomous4j.control.Brain;

import utils.KeyCommand;

public class Button extends JButton{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Brain b;
	private KeyCommand kc;
	    
    public Button(String string,Brain b){
		super(string);
    	this.b=b;
    	kc = new KeyCommand(b);
		addKeyListener(kc);
	}
}
