package gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import org.autonomous4j.control.Brain;

import javax.swing.JButton;
import javax.swing.JTextField;

public class pruebaKeyListener extends JFrame implements KeyListener{

	private JPanel contentPane;
	private JTextField textField;

	public void keyPressed(KeyEvent e) {
        System.out.println("keyPressed");
    }
    public void keyReleased(KeyEvent e) {
        if(e.getKeyCode()== KeyEvent.VK_RIGHT)
            System.out.println("derecha");
        else if(e.getKeyCode()== KeyEvent.VK_LEFT)
            System.out.println("izquierda");
        else if(e.getKeyCode()== KeyEvent.VK_DOWN)
            System.out.println("abajo");
        else if(e.getKeyCode()== KeyEvent.VK_UP)
            System.out.println("arriba");
    }
    public void keyTyped(KeyEvent e) {
        System.out.println("keyTyped");
    }
	
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		pruebaKeyListener frame = new pruebaKeyListener();
		frame.setVisible(true);

	}

	/**
	 * Create the frame.
	 */
	public pruebaKeyListener() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 828, 559);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		Brain b = new Brain();
		Button btnPrueba = new Button("prueba",b );
		btnPrueba.setBounds(530, 204, 115, 29);
		contentPane.add(btnPrueba);
		
		textField = new JTextField();
		textField.setBounds(75, 205, 146, 26);
		contentPane.add(textField);
		textField.setColumns(10);
		addKeyListener(this);
		setFocusable(true);
		setFocusTraversalKeysEnabled(false);
		
	}
}
